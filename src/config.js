export default {
  MAX_ATTACHMENT_SIZE: 5000000,
  s3: {
    REGION: "us-east-2",
    BUCKET: "notes-app-api-prod-serverlessdeploymentbucket-1165dz7mo9hpj"
  },
  apiGateway: {
    REGION: "us-east-2",
    URL: "https://in3lpthoxb.execute-api.us-east-2.amazonaws.com/prod"
  },
  cognito: {
    REGION: "us-east-2",
    USER_POOL_ID: "us-east-2_jfsALNoXp",
    APP_CLIENT_ID: "30o3j6l32lnaomv3l5ob8toogp",
    IDENTITY_POOL_ID: "us-east-2:2d06b2db-150b-4ef2-bf47-9ee8b51ada21"
  }
};